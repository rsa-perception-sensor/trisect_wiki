baseURL: "https://trisect-perception-sensor.gitlab.io/trisect-docs/"
title: "Trisect"
description: "Three-camera underwater perception sensor"

enableRobotsTXT: true

module:
  hugoVersion:
    extended: true
    min: "0.73.0"
  imports:
    - path: "github.com/google/docsy"
      disable: false
    - path: "github.com/google/docsy/dependencies"
      disable: false

# Will give values to .Lastmod etc.
enableGitInfo: true

# Language settings
# contentDir = "content/en"
# defaultContentLanguage = "en"
# defaultContentLanguageInSubdir = false
# Useful when translating.
# enableMissingTranslationPlaceholders = true

disableKinds: ["taxonomy", "RSS"]

# Highlighting config
pygmentsCodeFences: true
pygmentsUseClasses: false
# Use the new Chroma Go highlighter in Hugo.
pygmentsUseClassic: false
#pygmentsOptions = "linenos=table"
# See https://help.farbox.com/pygments.html
pygmentsStyle: "tango"

# Configure how URLs look like per section.
permalinks:
  blog: "/:section/:year/:month/:day/:slug/"

## Configuration for BlackFriday markdown parser: https://github.com/russross/blackfriday
blackfriday:
  plainIDAnchors: true
  hrefTargetBlank: true
  angledQuotes: false
  latexDashes: true

# Image processing configuration.
imaging:
  resampleFilter: "CatmullRom"
  quality: 75
  anchor: "smart"

# services:
#   googleAnalytics:
# Comment out the next line to disable GA tracking. Also disables the feature described in [params.ui.feedback].
# id = "UA-00000000-0"

# Language configuration

markup:
  goldmark:
    renderer:
      unsafe: true
    markup:
      highlight:
        # See a complete list of available styles at https://xyproto.github.io/splash/docs/all.html
        style: "tango"
        # Uncomment if you want your chosen highlight style used for code blocks without a specified language
        # guessSyntax = "true"
    extensions:
      passthrough:
        delimiters:
          block:
          - - \[
            - \]
          - - $$
            - $$
        enable: true

# Everything below this are Site Params

# Comment out if you don't want the "print entire section" link enabled.
outputs:
  section: ["HTML", "RSS"]

params:
  copyright: "University of Washington"

  site_logo: "/images/apl_logo_white_small.png"

  # Latex rendering is off by default
  # Enable in front matter for a page with "math: true"
  math: false

  # privacy_policy = "https://policies.google.com/privacy"

  # First one is picked as the Twitter card image if not set on page.
  # images = ["images/project-illustration.png"]

  # Menu title if your navbar has a versions selector to access old versions of your site.
  # This menu appears only if you have at least one [params.versions] set.
  # version_menu = "Releases"

  # Flag used in the "version-banner" partial to decide whether to display a
  # banner on every page indicating that this is an archived version of the docs.
  # Set this flag to "true" if you want to display the banner.
  archived_version: false

  # The version number for the version of the docs represented in this doc set.
  # Used in the "version-banner" partial to display a version number for the
  # current doc set.
  version: "1.0"

  # A link to latest version of the docs. Used in the "version-banner" partial to
  # point people to the main doc site.
  url_latest_version: "https://trisect-perception-sensor.gitlab.io/trisect-docs/"

  # Repository configuration (URLs for in-page links to opening issues and suggesting changes)
  github_repo: "https://gitlab.com/trisect-perception-sensor/trisect-docs"
  #github_project_repo = "https://gitlab.com/rsa-perception-sensor"
  github_branch: "published"

  # Disable Algolia DocSearch
  # search:
  #   algolia: false

  # Enable Lunr.js offline search
  offlineSearch: false

  # Enable syntax highlighting and copy buttons on code blocks with Prism
  prism_syntax_highlighting: false

#=========================================================================
# User interface configuration
  ui:
    # Enable to show the side bar menu in its compact state.
    sidebar_menu_compact: false
    #  Set to true to disable breadcrumb navigation.
    breadcrumb_disable: false
    #  Set to true to hide the sidebar search box (the top nav search box will still be displayed if search is enabled)
    sidebar_search_disable: false
    #  Set to false if you don't want to display a logo (/assets/icons/logo.svg) in the top nav bar
    navbar_logo: true
    # Set to true to disable the About link in the site footer
    footer_about_enable: True

  links:
    developer:
      - name: "GitLab"
        url: "https://gitlab.com/rsa-perception-sensor"
        icon: "fab fa-gitlab"
        desc: "Development takes place here!"

# # End user relevant links. These will show up on left side of footer and in the community page if you have one.
# # [[params.links.user]]
# # 	name = "User mailing list"
# #	url = "https://example.org/mail"
# #	icon = "fa fa-envelope"
# #       desc = "Discussion and help from your fellow users"
# # [[params.links.user]]
# #	name ="Twitter"
# #	url = "https://example.org/twitter"
# #	icon = "fab fa-twitter"
# #        desc = "Follow us on Twitter to get the latest news!"
# # [[params.links.user]]
# #	name = "Stack Overflow"
# #	url = "https://example.org/stack"
# #	icon = "fab fa-stack-overflow"
# #        desc = "Practical questions and curated answers"
# # Developer relevant links. These will show up on right side of footer and in the community page if you have one.
# #[[params.links.developer]]
# #	name = "Slack"
# #	url = "https://example.org/slack"
# #	icon = "fab fa-slack"
# #        desc = "Chat with other project developers"
# #[[params.links.developer]]
# #	name = "Developer mailing list"
# #	url = "https://example.org/mail"
# #	icon = "fa fa-envelope"
# #        desc = "Discuss development issues around the project"
