---
title: "Non-ROS Outputs"
weight: 40
description: The standard trisect_sw also exports data through a number of non-ROS interfaces
resource:
 - src: "*.jpg"
---

The primary data output from the Trisect software is via the [ROS Topics]({{< relref "ROS_Topics" >}}).

The standard stack also provides these outputs:

## Streaming video

The Trisect stack runs a customized [web_video_server](https://gitlab.com/rsa-perception-sensor/trisect_web_video_server) on port 8080.

## Rosbridge
