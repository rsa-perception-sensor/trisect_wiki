---
title: "Trisect Versions"
linkTitle: "Versions"
weight: 5
description: >
  Version of Trisect software/hardware
---

From a versioning perspective, Trisect consists of two major components, the [software]({{< ref "#software" >}}) which is in a state of continuous improvements, and the [hardware and board support package]({{< ref "#hardware" >}}) which changes at a slower pace.

----

## Software Versions {#software}

The userspace Trisect software is under continuous development, marked by releases in [`trisect_sw`](https://gitlab.com/rsa-perception-sensor/trisect_sw).  These releases are linked to the generations of [hardware]({{< ref "#hardware" >}}) partly because we tend to coordinate big, breaking software changes witha generational changes in the hardware, and partly because updates to Jetpack change underlying APIs.

### `trisect-6.1/release-3.x`

Latest deveopment version of `Dognight`.  This version is imminent as we test Jetpack 6.1 on the Drysect platform.

### `trisect-5.1.4/release-2.x`

The current development version for both `Bagsful` and `Crowd`.   Relative to `release-1.x` there's a lot of refactoring under the hood, resulting in performance improvements though not necessarily functional changes.

The largest change thusfar is moving image downsampling and rectification into `trisect_camera_driver`, where it can be GPU accelerated before being published as a ROS topic, though more changes to come.

VPI-accelerated block matching is currently not working in this version.  This is not for a very good reason --- development thusfar has used OpenCV 4.5.4 provided by NVidia, which *does not* have CUDA acceleration, but [gpu_stereo_image_proc_vpi](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc/tree/devel/orin_nx_drysect/gpu_stereo_image_proc_vpi) uses CUDA-accelerated filtering.   This could be fixed either by committing to building OpenCV from source with CUDA (as we did in `trisect-4.5.1`) or stripping out the OpenCV-CUDA code.

(Or dropping block matching support and going entirely to RAFT)

### `trisect-5.1.4/release-1.x`

This release corresponds with the the offline development on Jetpack 5.1.x of the Orin NX / Drysect / `Crowd` while the in-water Trisect platform was still at `Amigos`.  This is a brute-force forward-port of `trisect-4.5.1` to run on the newer BSP.   As we transitioned to `release-2.x` this was set aside and it never reached a stable point (but works, at least for running the cameras).

### `trisect-4.5.1`

The prior generation of Trisect software ([on Gitlab](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/tree/trisect-4.5.1?ref_type=heads)).   This is the generation used for all research to this point, it is tied to `Amigos` and, in its final form, used VPI 1.0 for stereo block matching.

----

## Hardware/BSP Versions {#hardware}


Generations of Trisect are marked by upgrades in either the underlying Jetson processor, and/or version of [NVidia Jetpack](https://developer.nvidia.com/embedded/jetpack) (since Trisect uses a custom kernel, upgrading between versions of Jetpack is non trivial).

The versions differ in how they are installed and bootstrapped.  Once setup, the [user-space software]({{< ref "Software_User" >}}) has a similar look-and-feel, although there are differences in content, performance, etc.

So far, there have been _three_ generations of Trisect hardware/BSP:

| Generation | Name    | Processor | Jetpack                                                                | Notes                                                                                                                                                   |
| ---------- | ------- | --------- | ---------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1          | [Amigos]({{< ref "Software_Amigos" >}}) | Xavier NX | [4.5.1](https://developer.nvidia.com/embedded/jetpack-sdk-451-archive) | First deployed generation, two built.                                                                                                                   |
| 2          | [Bagsful]({{< ref "Software_Bagsful" >}}) | Xaier NX  | [5.1.4](https://developer.nvidia.com/embedded/jetpack-sdk-514)         | Upgrade from Amigos in 2024.  One unit in benchtop testing at present.                                                                                  |
| 3          | [Crowd]({{< ref "Software_Bagsful" >}})   | Orin NX   | [5.1.4](https://developer.nvidia.com/embedded/jetpack-sdk-514)         | Parallel development with Bagsful with Seeed Studio J401 baseboard, which only supports two cameras.   At present we do not have a submersible version with Orin. |
| 4          | [Dognight]() | Orin NX | [6.1](https://developer.nvidia.com/embedded/jetpack-sdk-61) | This version will only support the Orin NX;  spurred by the release of the [Nano "Super"](https://developer.nvidia.com/blog/nvidia-jetson-orin-nano-developer-kit-gets-a-super-boost/) and the promised (forthcoming) updates to the Orin NVP models. |
