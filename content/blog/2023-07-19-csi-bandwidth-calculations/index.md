---
title: "CSI Bandwidth Calculations"
summary: "Estimation current CSI bandwidth usage"
date: 2023-07-19
resource:
 - src: "*.png"
---

I am starting to consider a migration path from the current Xavier NX to an Orin NX.  Pros would be even more GPU bandwidth, and two additional cores (if we get the [16GB Orin module](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/jetson-orin/)).  And tearing off the bandaid in terms of the latest hardware/Jetpack (Ubuntu 20.04 anyone?).

One step backwards in that conversion is that the Orin NX module has just 8 CSI lanes compared to 14 on the Xavier NX.   This would restrict us to 2x lanes for some of the cameras.   In truth the current Trisect design only uses 2 lanes for each camera -- due to some unresolved kernel issues -- but for the sake of argument let's see how much bandwidth we need.

Per [RidgeRun](https://developer.ridgerun.com/wiki/index.php/NVIDIA_Jetson_ISP_Control#MIPI_CSI_Bandwidth) CSI bandwidth is a simple product:

```
fps = (efficiency) * (#Lanes)*(Lane Speed in bps) /  ( (Width*Height) *(bpp) )
```

With a quoted efficiency of 0.85 for packet overhead, etc.  The Jetson NX/Orin DPHY has a bandwidth of 2.5Gbps per lane.

# Stereo cameras

The stereo cameras produce 8/10/12-bit 1936x1216 images.  When using 12-bit data it arrives as a simple 2-bytes per pixel (it isn't packed at all) so:

```
fps = 0.85 * (2 * 2.5e9) / (1936 * 1216 * 16)
    = 112.8 Hz
```

So clearly we don't need more bandwidth for the stereo sensors.

# Color cameras

The IMX477 can capture up to 4056 x 3040 at up to 30 fps.  It delivers data in Bayer RG10 format, which is also passed as 2 bytes per pixel (also un-packed).  So:

```
fps = 0.85 * (2 * 2.5e9) / (4056 * 3040 * 16)
    = 21.5 Hz
```

We aren't accessing the color camera at this rate, but here we might see an advantage in those extra lanes to capture color data at 24 or 30Hz.

We also need to consider the bandwidth of the Jetson ISP, but given encoding of 4k30 frames is a quoted capability of the Jetson, I assume it **could** run the camera at 30Hz full resolution.

For now it's not a major restriction.
