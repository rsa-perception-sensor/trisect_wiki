---
title: "Focusing Trisect"
summary: "Setting focus on the first Trisect camera"
date: 2022-02-03
resource:
 - src: "*.jpg"
---

Focus for the three Trisect cameras is set in the air, without the domes.  Once in the water, the domes act as diverging lenses, changing the effective focus distance and depth of field for the lenses.

I've added a detailed explanation to the [the documentation!]({{< ref "Focusing" >}})

{{< imgproc src="IMG_7588.jpg" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}
