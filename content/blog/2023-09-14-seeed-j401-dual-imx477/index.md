---
title: "Dual IMX477 on the Seeed J401 baseboard with an Orin NX"
summary: "Details on running two IMX477 (Raspberry Pi HQ cameras) on a Seeed Studio J401 baseboard with an Orin NX"
date: 2023-09-14
tags:
    - orin
resources:
 - src: "*.png"
 - src: "*.dts"
---

**Table of Contents:**

{{<toc>}}

I'm slowly working on a hardware upgrade for Trisect, with a couple of goals in minds --- more than anything else, updating to a more recent Jetpack and transitioning to VPI for the core stereo blockmatcher.

Since we didn't have any spare hardware for the current generation, I decided to upgrade to an Orin NX.  As there isn't actually an "Orin NX Development Kit," I instead purchased a [Seeed ReComputer J4012](https://www.seeedstudio.com/reComputer-J4012-p-5586.html?queryID=84c3e5066ce025ac295f33f63d7a2fdb&objectID=5586&indexName=bazaar_retailer_products) with an Orin NX 16GB.   This is basically a [J401 baseboard](https://www.seeedstudio.com/reComputer-J401-Carrier-Board-for-Jetson-Orin-NX-Orin-Nano-p-5636.html?queryID=84c3e5066ce025ac295f33f63d7a2fdb&objectID=5636&indexName=bazaar_retailer_products) in a case.   As a baseboard, the J401 is fairly similar to the official Orin **Nano** baseboard (part number p3678), at least close enough run the stock DTBs.

## CSI support on the Seeed Studio J401 baseboard

The CSI configuration on the J401 is slightly different, however, which means the "stock" DTB overlay for dual IMX477 / Raspberry Pi HQ cameras does not work.

[{{< imgproc src="orin_nano_csi_4lanes.png" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](orin_nano_csi_4lanes.png)

Looking at the [Orin Nano Developer Kit Carrier Board Specification](https://developer.download.nvidia.com/assets/embedded/secure/jetson/orin_nano/docs/Jetson_Orin_Nano_DevKit_Carrier_Board_Specification_SP-11324-001_v1.1.pdf?Z82e6hA-V2jPkAlgorrZwU-N08845WwiMLpphGi2IpG8no-HXjU15PMcM8168NdHptLprmFUy-LRzTz7b6Sfkbr4lqE3SvmHX7Hsk7_9tYxh6zVnndbPCo4E6eNjnAtzoOO7F5vo9tpoG1VuXFILyBpWhb1bAgMfsSlqSwbRuoZoEqE42ZgELLx1zbfAdkoMQijsixa7aKnxPdQKs2Y5D9v8hIKmkQ0iECYTOzjDz4mmbpriww==&t=eyJscyI6ImdzZW8iLCJsc2QiOiJodHRwczovL3d3dy5nb29nbGUuY29tLyJ9), in the section "3.2 Camera Connector", we see:

- Connector J20 is the **first output** from the I2C mux, so it's "Camera 0".  It includes:
  - Two lanes from CSI0
  - Two lanes from CSI1 and CSI1 clock

- Connector J21 is the **second output** from the I2C mux, so it's "Camera 1".  It includes:
  - Two lanes from CSI3
  - Two lanes from CSI2 and CSI2 clock

If we look at the [Orin Nano/NX Design Guide](https://developer.download.nvidia.com/assets/embedded/secure/jetson/orin_nx/docs/Jetson_Orin_NX_Series_and_Orin_Nano_Series_Design_Guide_DG-10931-001_v1.1.pdf?8GsG3yQ1wjDXZu7yw9AGql1QHcf2cjIb6S2_gvMDHV1Lr0gFoaNJnhB5a2yz2CNfQ5Z6Ttug68NXpQSUYRy0rSOIiss6blL7sUShScVDz3rynzoIE7Y2f9Mq06Z-tGDiuOgRx8JlUSGd1Z2UfupvwXXQn10DXl08_CMfngBGN9sApn9z7eqHObMiMcMsXhrtAkwPx5pc-OlVKkH8TYAfeQIWzbK7kBlkulE94-r_4kc-DuKE7XW2&t=eyJscyI6ImdzZW8iLCJsc2QiOiJodHRwczovL3d3dy5nb29nbGUuY29tLyJ9) however, we see a couple of things:

1. Lanes are swapped ("swizzled"?) for CSI0_D1 and CSI1_D0.  Why?  Dunno.   We can accommodate for this in the DTB, however.
2. If you want to run 4x mode you must use **CSI0** and **CSI2** clocks.  Since CSI0's clock is not included on J20, **you cannot do 4x lane on J20 on this baseboard.**   You can only do 4x lanes on J21.

[{{< imgproc src="j401_csi_connector.png" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}](j401_csi_connector.png)

Jumping over to the [J401 schematics](https://files.seeedstudio.com/wiki/J401/reComputer_J401_SCH_V1.0.pdf), we can see:

- J12, which they call "CAM1" is actually the first output from the I2C Mux, so it's really "Camera 0" from a software point of view, and it gets **only** the 2x lanes of CSI0.
- J9, which they call "CAM0" is actually the second output from the I2C mux, so it's really "Camera 1", and it gets 2x lanes from CSI2.

Starting from the assumption that the DTB overlay they provide for dual IMX477 on the devkit
(`tegra234-p3767-camera-p3768-imx477-dual.dts`) actually works on the devkit baseboard, we just need to:

- Ensure everyhing is 2x lanes only
- Use CSI0 rather than CSI1 for camera 0.
- Ensure we are "swizzling" for CSI0 (and CSI1 too, though we aren't using it)


----

## The actual DTB overlay

Here is the source for the DTB overlay which works for me: [tegra234-p3767-camera-j401-imx477-dual.dts]({{< page_resource_permalink "tegra234-p3767-camera-j401-imx477-dual.dts" >}})

I am not a `dtbo` expert -- to build this overlay I needed to add it to a full kernel tree.  I am working primarily with the [AlliedVision kernel](https://github.com/alliedvision/linux_nvidia_jetson/tree/l4t-35.4.1) which has a convenience wrapper for compiling the kernel and DTBs; if working directly with the Nvidia kernel there maybe a few more manual steps.   However, steps are something like:

1. Place the attached `.dts` file in the kernel source tree at `hardware/nvidia/platform/t23x/p3768/kernel-dts/`
2. Add a line to the Makefile in that directory to compile the `dtbo`:

```
dtbo-$(BUILD_ENABLE) += tegra234-p3767-camera-j401-imx477-dual.dtbo
```

3. Compile the device trees in the kernel.   This will compile all of the device trees and overlays (I'm sure there's a way to compile just the one overlay but I haven't dug into it).
4. Copy the the resulting overlay `tegra234-p3767-camera-j401-imx477-dual.dtbo` file to a running system, put it in `/boot`
5. Then apply to overlay.  Simplest way is to run `jetson-io`, either the graphical tool `jetson-io.py` or using `config-by-hardware.py`.  For example:

```
$ sudo jetson-io/config-by-hardware.py --list
Header 1 [default]: Jetson 40pin Header
  Available hardware modules:
  1. Adafruit SPH0645LM4H
  2. Adafruit UDA1334A
  3. FE-PI Audio V1 and Z V2
  4. ReSpeaker 4 Mic Array
  5. ReSpeaker 4 Mic Linear Array
Header 2: Jetson 24pin CSI Connector
  Available hardware modules:
  1. Camera IMX219 Dual
  2. Camera IMX477 Dual
  3. Camera IMX477 Dual 4 Lane
  4. Camera IMX477-A and IMX219-C
  5. [J401] Dual IMX477
Header 3: Jetson M.2 Key E Slot
  No hardware configurations found!

$ sudo jetson-io/config-by-hardware.py -n 2="[J401] Dual IMX477"
Configuration saved to /boot/kernel_tegra234-p3767-0000-p3768-0000-a0-user-custom.dtb.
```

`jetson-io.py` does the extra work of added a `/boot/extlinux/extlinux.conf` entry with the new FDT.

You can also do this manually with `fdtoverlay`, something like:

```
sudo fdtoverlay -i /boot/kernel_tegra234-p3767-0000-p3768-0000-a0.dtb -o /boot/kernel_tegra234-p3767-0000-p3768-0000-a0-user-custom.dtb /boot/tegra234-p3767-camera-j401-imx477-dual.dtbo
```

In this case, however, you will have to ensure the resulting `dtb` is references in `/boot/extlinux/extlinux.conf.`
