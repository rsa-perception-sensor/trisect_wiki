---
title: "Trisect emulator"
date: 2022-01-18
description: "I've written a short Trisect emulator / playback ROS node"
---

By request, I've written a small [Docker image](https://gitlab.com/rsa-perception-sensor/trisect_emulator) which acts as a Trisect emulator.   The container contains instances of:

* [trisect_web_video_server](https://gitlab.com/rsa-perception-sensor/trisect_web_video_server)
* [ros_bridge](http://wiki.ros.org/rosbridge_suite)
* [rostful](https://github.com/apl-ocean-engineering/rostful)

along with a script to download a sample bag and playback automatically.

Right now the sample bag only contains the three image streams (left monochrome, right monochrome, color), and no depth/point clouds.    As we go, we will continue to update both the software and the sample bag to be more accurate the true hardware.
